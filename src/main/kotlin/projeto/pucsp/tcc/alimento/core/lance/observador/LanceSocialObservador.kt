package projeto.pucsp.tcc.alimento.core.lance.observador

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component
import projeto.pucsp.tcc.alimento.core.lance.dominio.OrquestramentoLanceAlimento
import projeto.pucsp.tcc.alimento.core.lance.entidade.LanceMitigacao
import projeto.pucsp.tcc.alimento.core.lance.entidade.LanceSocial
import projeto.pucsp.tcc.alimento.core.lance.recurso.LanceRecurso


@Component
class LanceSocialObservador(
        val orquestramentoLanceAlimento: OrquestramentoLanceAlimento) : LanceRecurso {

    companion object {
        val log: Logger = LoggerFactory.getLogger(LanceSocialObservador::class.java)
    }

    @RabbitListener(queues = ["lance.alimento"])
    override fun cadastrarLanceAlimento(lanceSocial: LanceSocial) {

        log.info("processando lance de alimento para id social {}", lanceSocial.idSocial)

        orquestramentoLanceAlimento.processarLanceSocial(lanceSocial)

    }

    @RabbitListener(queues = ["mitigar.lance"])
    override fun mitigarLance(lanceMitigacao: LanceMitigacao) {

        log.info("mitigando lance com id {}", lanceMitigacao.codigo)

        orquestramentoLanceAlimento.mitigarLance(lanceMitigacao)

    }
}
package projeto.pucsp.tcc.alimento.core.lance.cliente

import projeto.pucsp.tcc.alimento.core.lance.entidade.LancePublicacao

interface ClienteLance {

    fun publicarLance(lancePublicacao: LancePublicacao)
}
package projeto.pucsp.tcc.alimento.core.lance

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import projeto.pucsp.tcc.alimento.core.lance.propriedades.PropriedadeNotificacao
import projeto.pucsp.tcc.alimento.core.lance.propriedades.PropriedadeProcessarLance

@EnableConfigurationProperties(PropriedadeProcessarLance::class, PropriedadeNotificacao::class)
@SpringBootApplication
class LanceApplication

fun main(args: Array<String>) {
	runApplication<LanceApplication>(*args)
}

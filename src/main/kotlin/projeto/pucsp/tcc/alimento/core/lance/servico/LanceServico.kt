package projeto.pucsp.tcc.alimento.core.lance.servico

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import projeto.pucsp.tcc.alimento.core.lance.cliente.ClienteLance
import projeto.pucsp.tcc.alimento.core.lance.entidade.Lance
import projeto.pucsp.tcc.alimento.core.lance.entidade.LanceMitigacao
import projeto.pucsp.tcc.alimento.core.lance.entidade.LancePublicacao
import projeto.pucsp.tcc.alimento.core.lance.entidade.Localizacao
import projeto.pucsp.tcc.alimento.core.lance.repositorio.LanceRepositorio

@Service
class LanceServico(val repositorio: LanceRepositorio, private val clienteLance: ClienteLance) {

    companion object {
        val log: Logger = LoggerFactory.getLogger(LanceServico::class.java)
    }

    fun cadastrarLance(lances: List<Lance>): MutableList<Lance> {
        log.info("#cadastrando lance : {}", lances.toString())
        return repositorio.saveAll(lances)
    }

    fun publicarLance(lances: List<Lance>, localizacao: Localizacao) {

        log.info("#publicando lance : {}", lances.toString())

        lances
                .parallelStream()
                .map { lance: Lance -> LancePublicacao(lance, localizacao) }
                .forEach { lancePublicacao: LancePublicacao -> clienteLance.publicarLance(lancePublicacao) }

    }

    fun mitigarLance(lanceMitigacao: LanceMitigacao) {

        repositorio.findById(lanceMitigacao.codigo).ifPresent { lance: Lance ->

            lance.mitigar(lanceMitigacao)

            log.info("#mitigando lance : {}", lance.toString())

            repositorio.save(lance)
        }
    }
}
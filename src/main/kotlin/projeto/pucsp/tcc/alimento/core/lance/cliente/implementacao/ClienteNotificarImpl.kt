package projeto.pucsp.tcc.alimento.core.lance.cliente.implementacao

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Component
import projeto.pucsp.tcc.alimento.core.lance.cliente.ClienteNotificar
import projeto.pucsp.tcc.alimento.core.lance.entidade.NotificacaoPublicacao
import projeto.pucsp.tcc.alimento.core.lance.propriedades.PropriedadeNotificacao
import projeto.pucsp.tcc.alimento.core.lance.servico.LanceServico

@Component
class ClienteNotificarImpl(
        private val rabbitTemplate: RabbitTemplate,
        private val propriedadeNotificacao: PropriedadeNotificacao) : ClienteNotificar {

    companion object {
        val log: Logger = LoggerFactory.getLogger(LanceServico::class.java)
    }

    override fun encaminhar(notificacaoPublicacao: NotificacaoPublicacao) {

        log.info("encaminhar notificação : {}", notificacaoPublicacao)

        rabbitTemplate.convertAndSend(propriedadeNotificacao.topicExchange, notificacaoPublicacao)

    }
}
package projeto.pucsp.tcc.alimento.core.lance.dominio

import org.springframework.stereotype.Component
import projeto.pucsp.tcc.alimento.core.lance.cliente.ClienteNotificar
import projeto.pucsp.tcc.alimento.core.lance.entidade.*
import projeto.pucsp.tcc.alimento.core.lance.processo.Processo
import projeto.pucsp.tcc.alimento.core.lance.servico.LanceServico

@Component
class OrquestramentoLanceAlimento(
        private val processo: Processo,
        private val lanceServico: LanceServico,
        private val clienteNotificar: ClienteNotificar) {

    fun processarLanceSocial(lanceSocial: LanceSocial) {

        val lances = processo.lanceSocialParaListaDeLances(lanceSocial)

        val lancesCadastrados = lanceServico.cadastrarLance(lances)

        notificarLance(lanceSocial)
                .publicarProcessarLance(lancesCadastrados, lanceSocial.localizacao)

    }

    fun mitigarLance(lanceMitigacao: LanceMitigacao) {
        lanceServico.mitigarLance(lanceMitigacao)
    }

    private fun notificarLance(lanceSocial: LanceSocial): OrquestramentoLanceAlimento {

        clienteNotificar.encaminhar(
                NotificacaoPublicacao("Recebemos seu lance de alimento. Assim que tivermos novidades, você receberá um email informativo",
                        "Lance de alimento recebido",
                        lanceSocial.emailSocial, lanceSocial.alimentos.map { alimento: Alimento ->  alimento.codigo}))

        return this
    }

    private fun publicarProcessarLance(lances: List<Lance>, localizacao: Localizacao): OrquestramentoLanceAlimento {

        lanceServico.publicarLance(lances, localizacao)

        return this

    }
}
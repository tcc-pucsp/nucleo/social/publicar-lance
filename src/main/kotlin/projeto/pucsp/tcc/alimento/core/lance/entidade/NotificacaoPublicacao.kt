package projeto.pucsp.tcc.alimento.core.lance.entidade

data class NotificacaoPublicacao(val mensagem: String, val assunto: String, val endereco: String, val codigosAlimento: List<Int>)
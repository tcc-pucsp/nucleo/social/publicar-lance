package projeto.pucsp.tcc.alimento.core.lance.entidade

import lombok.Value
import kotlin.properties.Delegates

@Value
class LanceSocial {

    lateinit var emailSocial: String

    var idSocial by Delegates.notNull<Int>()

    lateinit var alimentos: List<Alimento>

    lateinit var localizacao: Localizacao
    
}
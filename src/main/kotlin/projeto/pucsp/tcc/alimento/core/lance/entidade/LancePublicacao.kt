package projeto.pucsp.tcc.alimento.core.lance.entidade

data class LancePublicacao(val lanceId: Int,
                           val socialId: Int,
                           val alimentoId: Int,
                           val quantidadeAlimento: Int,
                           val localizacao: Localizacao) {
    constructor(lance: Lance, localizacao: Localizacao) : this(
            lance.id,
            lance.socialId,
            lance.alimentoId,
            lance.quantidadeAlimento,
            localizacao
    )
}
package projeto.pucsp.tcc.alimento.core.lance.processo

import org.springframework.stereotype.Service
import projeto.pucsp.tcc.alimento.core.lance.entidade.Lance
import projeto.pucsp.tcc.alimento.core.lance.entidade.LanceSocial
import java.util.stream.Collectors

@Service
class LanceProcesso : Processo {

    override fun lanceSocialParaListaDeLances(lanceSocial: LanceSocial): List<Lance> {
        return lanceSocial
                .alimentos
                .parallelStream()
                .map { alimento -> Lance(lanceSocial.idSocial, alimento)  }
                .collect(Collectors.toList())
    }

}
package projeto.pucsp.tcc.alimento.core.lance.recurso

import projeto.pucsp.tcc.alimento.core.lance.entidade.LanceMitigacao
import projeto.pucsp.tcc.alimento.core.lance.entidade.LanceSocial

interface LanceRecurso {

    fun cadastrarLanceAlimento(lanceSocial: LanceSocial)

    fun mitigarLance(lanceMitigacao: LanceMitigacao)
}
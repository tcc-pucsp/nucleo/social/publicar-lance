package projeto.pucsp.tcc.alimento.core.lance.entidade

import lombok.Value

@Value
class Localizacao {

    lateinit var latitude: String

    lateinit var longitude: String

}

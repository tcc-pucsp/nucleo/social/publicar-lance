package projeto.pucsp.tcc.alimento.core.lance.entidade

import lombok.Value
import kotlin.properties.Delegates

@Value
class LanceMitigacao {

    var codigo by Delegates.notNull<Int>()

    var quantidade by Delegates.notNull<Int>()

}
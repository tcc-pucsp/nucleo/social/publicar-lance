package projeto.pucsp.tcc.alimento.core.lance.processo

import projeto.pucsp.tcc.alimento.core.lance.entidade.Lance
import projeto.pucsp.tcc.alimento.core.lance.entidade.LanceSocial

interface Processo {

    fun lanceSocialParaListaDeLances(lanceSocial: LanceSocial) : List<Lance>
}
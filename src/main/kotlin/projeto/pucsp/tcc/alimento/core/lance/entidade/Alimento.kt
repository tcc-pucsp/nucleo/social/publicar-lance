package projeto.pucsp.tcc.alimento.core.lance.entidade

import lombok.Value
import kotlin.properties.Delegates

@Value
class Alimento {

    var codigo by Delegates.notNull<Int>()

    var quantidade by Delegates.notNull<Int>()

}
package projeto.pucsp.tcc.alimento.core.lance.entidade

import lombok.Data
import javax.persistence.*


@Entity
@Table(name = "lance")
@Data
data class Lance(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int,

        val socialId: Int,

        val alimentoId: Int,

        var quantidadeAlimento: Int,

        var situacaoLance: Int

) {
    fun mitigar(lanceMitigacao: LanceMitigacao) {

        quantidadeAlimento += lanceMitigacao.quantidade

        if (quantidadeAlimento > 0) {
            situacaoLance = 1
        }

    }

    constructor(idSocial: Int, alimento: Alimento) : this(0, idSocial, alimento.codigo, alimento.quantidade, 0)
}
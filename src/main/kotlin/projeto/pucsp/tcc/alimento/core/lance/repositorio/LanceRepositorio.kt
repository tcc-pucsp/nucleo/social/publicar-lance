package projeto.pucsp.tcc.alimento.core.lance.repositorio

import org.springframework.data.jpa.repository.JpaRepository
import projeto.pucsp.tcc.alimento.core.lance.entidade.Lance

interface LanceRepositorio: JpaRepository<Lance, Int>
package projeto.pucsp.tcc.alimento.core.lance.cliente

import projeto.pucsp.tcc.alimento.core.lance.entidade.NotificacaoPublicacao

interface ClienteNotificar {

    fun encaminhar(notificacaoPublicacao: NotificacaoPublicacao)

}

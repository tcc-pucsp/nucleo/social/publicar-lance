package projeto.pucsp.tcc.alimento.core.lance.propriedades

import lombok.Getter
import lombok.Setter
import lombok.ToString
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.validation.annotation.Validated

@ToString
@Setter
@Getter
@ConfigurationProperties(prefix = "rabbit.notificar")
@Validated
class PropriedadeNotificacao {

    lateinit var topicExchange: String

}
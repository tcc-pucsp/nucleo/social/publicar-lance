package projeto.pucsp.tcc.alimento.core.lance.cliente.implementacao

import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Component
import projeto.pucsp.tcc.alimento.core.lance.cliente.ClienteLance
import projeto.pucsp.tcc.alimento.core.lance.entidade.LancePublicacao
import projeto.pucsp.tcc.alimento.core.lance.propriedades.PropriedadeProcessarLance

@Component
class ClienteLanceRabbitImpl(
        private val rabbitTemplate: RabbitTemplate,
        private val propriedadeProcessarLance: PropriedadeProcessarLance) : ClienteLance {

    override fun publicarLance(lancePublicacao: LancePublicacao) {

        rabbitTemplate.convertAndSend(propriedadeProcessarLance.topicExchange, lancePublicacao)

    }

}
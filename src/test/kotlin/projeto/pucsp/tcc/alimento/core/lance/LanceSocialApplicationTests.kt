package projeto.pucsp.tcc.alimento.core.lance

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import projeto.pucsp.tcc.alimento.core.lance.entidade.Alimento
import projeto.pucsp.tcc.alimento.core.lance.entidade.LanceSocial
import projeto.pucsp.tcc.alimento.core.lance.entidade.Localizacao
import projeto.pucsp.tcc.alimento.core.lance.observador.LanceSocialObservador
import projeto.pucsp.tcc.alimento.core.lance.repositorio.LanceRepositorio

@ActiveProfiles("test")
@RunWith(SpringRunner::class)
@SpringBootTest
class LanceSocialApplicationTests {

    @Autowired
    private val lanceSocialObservador: LanceSocialObservador? = null

    @Autowired
    private val repositorio: LanceRepositorio? = null

    @Test
    fun contextLoads() {

        val lanceSocial = LanceSocial()

        lanceSocial.idSocial = 1
        lanceSocial.emailSocial = "email@email.com"

        val localizacao = Localizacao()

        localizacao.latitude = "-23.5552073"
        localizacao.longitude = "-46.7418157"

        lanceSocial.localizacao = localizacao

        val alimento = Alimento()

        alimento.codigo = 1
        alimento.quantidade = 2

        lanceSocial.alimentos = listOf(alimento)

        lanceSocialObservador?.cadastrarLanceAlimento(lanceSocial)

        repositorio?.existsById(1)?.let { assert(it) }

    }

}

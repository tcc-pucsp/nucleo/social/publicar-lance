package projeto.pucsp.tcc.alimento.core.lance.config

import com.rabbitmq.client.Channel
import org.mockito.Mockito
import org.springframework.amqp.rabbit.connection.Connection
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile


@Profile("test")
@Configuration
class RabbitMqMockConfig {

    @Bean
    fun connectionFactory(): ConnectionFactory {

        val connectionFactory = Mockito
                .mock(ConnectionFactory::class.java)

        val connection = connection()

        Mockito
                .`when`(connectionFactory.createConnection())
                .thenReturn(connection)

        Mockito
                .`when`(connection.isOpen)
                .thenReturn(true)

        Mockito
                .`when`(connection.createChannel(Mockito.anyBoolean()))
                .thenReturn(Mockito.mock(Channel::class.java))

        return connectionFactory

    }

    private fun connection(): Connection {

        return Mockito.mock(Connection::class.java)

    }

}